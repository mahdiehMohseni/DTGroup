package com.example.mahdieh.dtgroup.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mahdieh.dtgroup.Class.FlightInfoActivity;
import com.example.mahdieh.dtgroup.Class.NewResultActivity;
import com.example.mahdieh.dtgroup.Class.ResultActivity;
import com.example.mahdieh.dtgroup.Model.Flight;
import com.example.mahdieh.dtgroup.Model.FlightItem;
import com.example.mahdieh.dtgroup.Model.PostSearchDataEntry;
import com.example.mahdieh.dtgroup.R;
import com.gc.materialdesign.views.Card;
import org.w3c.dom.Text;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by mahdieh on 5/11/2016.
 */
public class ResultActivityAdapter extends RecyclerView.Adapter<ResultActivityAdapter.flightViewHolder> {
    //================== Global Var
    Context context;
    List<Flight> flightItems ;
    private OnItemClick itemClick;
    //=================== Constructor  ============================= //
    public ResultActivityAdapter(Context context, List<Flight> flightItems) {
        this.flightItems = flightItems;
        this.context = context;
    }
   //===================  ViewHolder ============================= //
    public class flightViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

       @Bind(R.id.resultActivityCardView)
       CardView ResultCardView;
        @Bind(R.id.resultActivityImageViewArrow)
       ImageView ArrowKey;
        @Bind(R.id.resultActivityTextViewCode)
               TextView num_flight;
        @Bind(R.id.resultActivityTextViewAgencyname)
       TextView airline;
        @Bind(R.id.resultActivityTextViewDate)
       TextView week;
        @Bind(R.id.resultActivityTextViewSource)
       TextView startTripCity;
        @Bind(R.id.resultActivityTextViewDestination)
       TextView DestTripCity;
        @Bind(R.id.resultActivityTextViewStartTime)
       TextView Start_time;
        @Bind(R.id.resultActivityTextViewEndTime)
       TextView End_time;
        @Bind(R.id.resultActivityTextViewPricing)
       TextView price;
        @Bind(R.id.resultActivityTypeRequest)
       TextView Carrier;
        @Bind(R.id.resultActivityTextViewLimitedAlert)
       TextView capacity;
        @Bind(R.id.resultActivityTextViewRibbon)
               TextView resultRibbon;
        @Bind(R.id.Shopping_badge_textView)
              TextView resultRibbonBadge;

        flightViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

      //=================== Sending value from recycler view to next Activity  ============================= //
       @Override
       public void onClick(View v) {
       }
    }
    //=================== OnCreateViewHolder ============================= //
    public flightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                return new flightViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.result_item_full,parent, false));
            case 1:
                return new flightViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.result_view_limiteded,parent, false));
            default:
            case 2:
                return new flightViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.result_item_j,parent, false));
        }
     }

    @Override
    public int getItemViewType(int position) {
        Flight flight = flightItems.get(position);
        if(flight.getCapacity()>0){
            if(flight.getCapacity() > 10){
                return 2;
            }else{
                return 1;
            }
        }else {
            return 0;
        }
    }
    //=================== onBindViewHolder ============================= //
    @Override
    public void onBindViewHolder(flightViewHolder holder, int position) {
        final Flight current_flight = flightItems.get(position);
        CardView ResultCardView= holder.ResultCardView;
        holder.airline.setText(current_flight.getAirline());
        holder.num_flight.setText(current_flight.getNum_flight());
        holder.Start_time.setText(current_flight.getTime());
        holder.End_time.setText(current_flight.getTime());
        holder.capacity.setText(String.valueOf(current_flight.getCapacity()));
        holder.ArrowKey.setImageResource(R.drawable.circledleft);
        holder.Carrier.setText(current_flight.getCarrier());
        //holder.Carrier.setText(String.valueOf(current_flight.getType()));
        //==============destination / source / date **************************
        holder.DestTripCity.setText(current_flight.getDestTripCity());
        holder.startTripCity.setText(current_flight.getStartTripCity());
        holder.week.setText(current_flight.getWeek());
//===============???????????????????????????? ********************************
        switch (getItemViewType(position)){
            case 0://full
                holder.resultRibbon.setBackgroundResource(R.drawable.rounded_ribbon_full);
                holder.resultRibbonBadge.setBackgroundResource(R.drawable.shopping_card_badge_full);
                holder.price.setText(R.string.FullAlertCapacity);
            case 1://limited
                holder.capacity.setText(String.valueOf(current_flight.getCapacity()));
                holder.resultRibbon.setBackgroundResource(R.drawable.rounded_ribbon);
                holder.resultRibbonBadge.setBackgroundResource(R.drawable.shopping_cart_badge);
                holder.price.setText(current_flight.getPrice());
            default:
            case 2://normal
                holder.resultRibbon.setBackgroundResource(R.drawable.rounded_ribbon);
                holder.resultRibbonBadge.setBackgroundResource(R.drawable.shopping_cart_badge);
                holder.price.setText(current_flight.getPrice());
        }
    }

   @Override
    public int getItemCount() {
        return flightItems.size();
    }
}

//references
/*
http://stackoverflow.com/questions/36935709/indexoutofboundsexception-when-trying-to-populate-data-in-recyclerview-from-two
//http://stackoverflow.com/questions/36499177/sending-image-from-recycler-view-to-other-activity

 */