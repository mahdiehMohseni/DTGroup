package com.example.mahdieh.dtgroup.Class;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by mahdieh on 5/1/2016.
 */
public class ContactUsActivity extends AppCompatActivity {

    String phone = "09124879343";

    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));


    @Override
    public void startActivity(Intent intent) {
        this.intent = intent;

        super.startActivity(intent);
    }
}

//references
//http://stackoverflow.com/questions/5403308/make-a-phone-call-click-on-a-button

