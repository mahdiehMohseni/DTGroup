package com.example.mahdieh.dtgroup.Class;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.Adapter.SideBarAdapter;
import com.example.mahdieh.dtgroup.R;

import java.lang.reflect.Field;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by mahdieh on 5/25/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.navdrawer)
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    //*****
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    //************
    protected Typeface tfSocial;
    protected Typeface tfIcon;
    protected Typeface tf;
    protected Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = BaseActivity.this;
        ButterKnife.bind(this);

        //====initialize mTitles
        String[] mTitles = getResources().getStringArray(R.array.sideBarItemTitles);

        // add header
        View header = getLayoutInflater().inflate(R.layout.new_drawer_header, mDrawerList, false);
        mDrawerList.addHeaderView(header);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_ab_drawer);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            toolbar.setTitleTextColor(Color.WHITE);

            TextView toolbarTiltle = getToolbarTextView();
            if ( toolbarTiltle != null )
                toolbarTiltle.setTypeface(tf);
        }

            //set adapter on listView
            mDrawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mTitles));
            //-----------------------------
            mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2position, long arg3id) {
                    switch (arg2position) {
                        case 0:
                            startActivity(new Intent(BaseActivity.this, IntroductionActivity.class));
                            break;
                        case 1:
                            startActivity(new Intent(BaseActivity.this, PolicyActivity.class));
                            break;
                        case 2:
                            startActivity(new Intent(BaseActivity.this, ContactUsActivity.class));
                            break;
                        case 3:
                            startActivity(new Intent(BaseActivity.this, SocialContactActivity.class));
                            break;
                        case 4:
                            startActivity(new Intent(BaseActivity.this, BugReportActivity.class));
                            break;
                        default:
                            break;
                    }
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                }
            });

             mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    getActionBar().setTitle(R.string.app_name);
                }

                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    getActionBar().setTitle(R.string.app_name);
                }
            };
            //set the drawer toggle as the drawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        }



    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //sync the toggle state after onRestoreInstanceState
        mDrawerToggle.syncState();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        //pass the event to ActionBarDrawerToggle , if it returns true
        if (mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        //handle your other actionBar items
        return super.onOptionsItemSelected(item);
    }

//==== getToolbarTextView
    protected TextView getToolbarTextView() {
        TextView titleTextView = null;
        try {
            Field f = toolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(toolbar);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
        return titleTextView;
    }
}





