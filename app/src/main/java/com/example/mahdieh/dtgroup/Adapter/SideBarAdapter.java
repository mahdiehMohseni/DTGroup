package com.example.mahdieh.dtgroup.Adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.R;

//import butterknife.BindView;

/**
 * Created by mahdieh on 5/1/2016.
 */
public class SideBarAdapter extends ArrayAdapter <String> {

    /**
     * Created by mahdieh on 4/30/2016.
     */
        private Context context;
        private String[] titles;
        private String[] icons;
        private int resourceId;
        private Typeface tf, tfIcon;



        public SideBarAdapter(Context context, int resourceId, String[] titles, String[] icons, Typeface tf, Typeface tfIcon) {
            super(context, resourceId, titles);
            this.titles=titles;
            this.icons=icons;
            this.context=context;
            this.resourceId = resourceId;
            this.tf = tf;
            this.tfIcon = tfIcon;
        }

        public View getView (int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(resourceId, parent, false);

            TextView textView = (TextView) rowView.findViewById(R.id.sidebarItemTextView);
            textView.setText(titles[position]);
            textView.setTypeface(tf);


            TextView textViewIcon = (TextView) rowView.findViewById(R.id.sidebarItemTextViewIcon);
            textViewIcon.setText(icons[position]);
            textViewIcon.setTypeface(tfIcon);

            return  rowView;
              }

    }
