package com.example.mahdieh.dtgroup.Class;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.R;

/**
 * Created by mahdieh on 5/2/2016.
 */
public class DrawerItemHolder extends RecyclerView.ViewHolder {

    public ImageView  itemIcon;
    public TextView itemText;
    public DrawerItemHolder(View itemView) {
        super(itemView);
        itemIcon= (ImageView) itemView.findViewById(R.id.drawer_icon);
        itemText= (TextView) itemView.findViewById(R.id.drawer_text);
    }
}