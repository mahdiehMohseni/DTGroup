package com.example.mahdieh.dtgroup.Class;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mahdieh.dtgroup.R;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Created by mahdieh on 5/31/2016.
 */
public class FlightBillingActivity extends AppCompatActivity{
    //=====local variable
    Typeface tf;
    String flightBilling_AdultSpinner, flightBilling_KidsSpinner, flightBilling_BabySpinner,
            flightBilling_DestSpinner, flightBilling_SourceSpinner, flightBilling_Date,
            flightBilling_airline, flight_billing_ApiLink, flightBilling_Time,flightBilling_price;
    //==============Bind Variables
    @Bind(R.id.resultActivityImageViewArrow)
    ImageView ImageViewArrow;
    @Bind(R.id.activity_billing_textview_PriceUnit)
    TextView textview_PriceUnit;
    @Bind(R.id.activity_flight_billing_sourceCity)
    TextView flight_billing_TextView_sourceCity;
    @Bind(R.id.flight_billing_TextViewDestCity)
    TextView TextViewDestCity;
    @Bind(R.id.flight_billing_TextViewAirline)
    TextView flight_billing_TextViewAirline;
    @Bind(R.id.flight_billing_TextViewDate)
    TextView flight_billing_TextViewDate;
    @Bind(R.id.flight_billing_TextViewTime)
    TextView flight_billing_TextViewTime;
    //------ CardViewOne
    @Bind(R.id.Flight_Billing_Title_CardViewOne)
    TextView Flight_Billing_Title_CardViewOne;
    @Bind(R.id.activity_flightInfo_standMan)
    ImageView activity_flightBilling_standManIcon;
    @Bind(R.id.activity_billing_adultCount)
    TextView Flightbilling_adultCount_TextView;
    @Bind(R.id.activity_billing_kidsIcon)
    ImageView Flightbilling_KidsCountIcon;
    @Bind(R.id.activity_billing_KidsCount)
    TextView activity_billing_KidsCount_TextView;
    @Bind(R.id.activity_billing_BabyIcon)
    ImageView Flightbilling_BabyCountIcon;
    @Bind(R.id.activity_billing_BabyCount)
    TextView activity_billing_BabyCount_TextView;
    @Bind(R.id.resultActivityImageVieCoin)
    ImageView ImageViewCoin;
    @Bind(R.id.activity_billing_textview_PriceTitle)
    TextView textview_PriceTitle;
    @Bind(R.id.activity_billing_textview_Price)
    TextView textview_Price;
    //------
    @Bind(R.id.billingActivitySubmitBillingButton)
    Button SubmitBillingButton;
    //===========================
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_billing_three_person);
        ButterKnife.bind(this);
        //===== setTypesFaceForALL ====
        setTypesFaceForALL();
        tf = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile.ttf");
        //==============================
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //================ get value using intent
            flightBilling_AdultSpinner = extras.getString("Adult_spinner_result");
            flightBilling_KidsSpinner = extras.getString("Kids_spinner_result");
            flightBilling_BabySpinner = extras.getString("Baby_spinner_result");
            flightBilling_DestSpinner = extras.getString("flight_dest");
            Log.d("ttt", flightBilling_DestSpinner);
            flightBilling_SourceSpinner = extras.getString("flight_source");
            flightBilling_Date = extras.getString("flight_Date");
            //************************
            flightBilling_airline = extras.getString("flight_airline");
            flightBilling_Time = extras.getString("flight_startTime");
            flightBilling_price = extras.getString("flight_price");
            flight_billing_ApiLink = extras.getString("flight_payment_Link");
//initialize and set values ==========================================
            flight_billing_TextView_sourceCity.setText(flightBilling_SourceSpinner);
            TextViewDestCity.setText(flightBilling_DestSpinner);
            flight_billing_TextViewAirline.setText(flightBilling_airline);
            flight_billing_TextViewDate.setText(flightBilling_Date);
            flight_billing_TextViewTime.setText(flightBilling_Time);
            textview_Price.setText(flightBilling_price);
            Flightbilling_adultCount_TextView.setText(flightBilling_AdultSpinner);
            activity_billing_KidsCount_TextView.setText(flightBilling_KidsSpinner);
            activity_billing_BabyCount_TextView.setText(flightBilling_BabySpinner);
            //static values----------------
            Flight_Billing_Title_CardViewOne.setText(getResources().getString(R.string.Flight_Billing_Title));
            activity_flightBilling_standManIcon.setBackgroundResource(R.drawable.standing_man);
            Flightbilling_KidsCountIcon.setBackgroundResource(R.drawable.kid);
            Flightbilling_BabyCountIcon.setBackgroundResource(R.drawable.baby);
            ImageViewArrow.setBackgroundResource(R.drawable.circledleft);
            textview_PriceTitle.setText(getResources().getString(R.string.priceOfBilling));
            ImageViewCoin.setBackgroundResource(R.drawable.ic_coins);
        }
    }

    //=======  openBrowser ==========
   @OnClick(R.id.billingActivitySubmitBillingButton)
   public void openBrowser() {
       String url = "http://charter724.ir/" + flight_billing_ApiLink;
       Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
       startActivity(Intent.createChooser(i, "Choose Browser"));
   }
    //========== setTypesFaceForALL body of method
    private void setTypesFaceForALL() {
        flight_billing_TextView_sourceCity.setTypeface(tf);
        TextViewDestCity.setTypeface(tf);
        flight_billing_TextViewAirline.setTypeface(tf);
        flight_billing_TextViewDate.setTypeface(tf);
        flight_billing_TextViewTime.setTypeface(tf);
        textview_Price.setTypeface(tf);
        Flightbilling_adultCount_TextView.setTypeface(tf);
        activity_billing_KidsCount_TextView.setTypeface(tf);
        activity_billing_BabyCount_TextView.setTypeface(tf);
        Flight_Billing_Title_CardViewOne.setTypeface(tf);
        textview_PriceTitle.setTypeface(tf);

    }
}


