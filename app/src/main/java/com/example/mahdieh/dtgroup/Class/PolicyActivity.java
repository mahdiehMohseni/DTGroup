package com.example.mahdieh.dtgroup.Class;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.Adapter.PolicyViewAdapter;
import com.example.mahdieh.dtgroup.Model.PolicyItem;
import com.example.mahdieh.dtgroup.R;

import java.util.Vector;



/**
 * Created by mahdieh on 5/2/2016.
 */
public class PolicyActivity extends MainActivity {

    private ListView listView;
    private View policyView;

    private Vector<PolicyItem> policies;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        LayoutInflater inflater = getLayoutInflater();
        policyView = inflater.inflate(R.layout.activity_policy,linearLayout, true);
        linearLayout.setTag(PolicyActivity.class);
        toolbar.setTitle(getString(R.string.policyRules));


        policies = new Vector<>();

        String [] PolicyCategories = getResources().getStringArray(R.array.PolicyCategories);

        //add category
        PolicyItem sec1 = new PolicyItem();
        sec1.category= PolicyCategories[0];
        sec1.content_policy=getResources().getStringArray(R.array.Section1);

        PolicyItem sec2 = new PolicyItem();
        sec2.category = PolicyCategories[1];
        sec2.content_policy=getResources().getStringArray(R.array.Section2);


        policies.add(sec1);
        policies.add(sec2);

        //-------assign view to listview bt adapter
        listView = (ListView) policyView.findViewById(R.id.policyActivityListView);
        listView.setAdapter(new PolicyViewAdapter(PolicyActivity.this,R.layout.activity_policy_parent, policies, tf,tfIcon, null));
        setTypefaceForAllView();
    }

    private void setTypefaceForAllView() {

        ((TextView) policyView.findViewById(R.id.policyActivityTextViewTitle)).setTypeface(tf);

    }
}
