package com.example.mahdieh.dtgroup.Adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import com.example.mahdieh.dtgroup.Class.IntroductionPagerFragment;
import com.example.mahdieh.dtgroup.Class.IntroductionViewPager;
import com.example.mahdieh.dtgroup.Other.DataStorage;
import com.example.mahdieh.dtgroup.R;

/**
 * Created by mahdieh on 5/3/2016.
 */
public class IntroViewPagerAdapter extends FragmentPagerAdapter {

    protected static String[] TITLE;
    protected static String[] Description;
    protected static TypedArray Icons;

    public IntroViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        TITLE = context.getResources().getStringArray(R.array.introductionPagerTitles);
        Description = context.getResources().getStringArray(R.array.introductionPagerDescription);
        Icons = context.getResources().obtainTypedArray(R.array.introductionPagerIcons);
    }


    @Override
    public int getCount() {
        return DataStorage.introductionPageCount;
    }

    @Override
    public Fragment getItem(int position) {
        return IntroductionPagerFragment.newInstance(TITLE[TITLE.length - position - 1],
                Description[Description.length - position - 1], Icons.getResourceId(Icons.length() - position - 1, 0));


    }


}
