package com.example.mahdieh.dtgroup.Class;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.Adapter.ListViewAdapter;
import com.example.mahdieh.dtgroup.Model.IntroItem;
import com.example.mahdieh.dtgroup.R;

import java.util.Vector;

/**
 * Created by mahdieh on 5/1/2016.
 */
public class IntroductionActivity extends MainActivity  {
    private View IntroductionView;
    private ListView listView;

    private Vector<IntroItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        LayoutInflater inflater = getLayoutInflater();
        IntroductionView = inflater.inflate(R.layout.activity_introduction, linearLayout, true);
        linearLayout.setTag(IntroductionActivity.class);
        items = new Vector<>();
        // ---define categories
        String [] IntroCategories = getResources().getStringArray(R.array.introCategories);

        // add section to categories
        IntroItem sec1 = new IntroItem();
        sec1.category = IntroCategories[0];
        sec1.intro_content = getResources().getStringArray(R.array.Intro_section1);

        IntroItem sec2 = new IntroItem();
        sec2.category = IntroCategories[1];
        sec2.intro_content = getResources().getStringArray(R.array.Intro_section2);

        IntroItem sec3 = new IntroItem();
        sec3.category = IntroCategories[2];
        sec3.intro_content=getResources().getStringArray(R.array.Intro_section3);

        //-----------------------------
        items.add(sec1);
        items.add(sec2);
        items.add(sec3);

        //------assign view to listview by adapter------
        listView = (ListView) IntroductionView.findViewById(R.id.IntroActivityListView);
        listView.setAdapter(new ListViewAdapter(IntroductionActivity.this, R.layout.activity_intro_parent, items, tf, tfIcon, null));

        setTypeFaceForAllViews();

    }

    private void setTypeFaceForAllViews() {
        ((TextView) IntroductionView.findViewById(R.id.IntroActivityTextTitle)).setTypeface(tf);
    }


}


