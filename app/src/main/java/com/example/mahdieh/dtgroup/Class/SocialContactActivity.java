package com.example.mahdieh.dtgroup.Class;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.mahdieh.dtgroup.R;

/**
 * Created by mahdieh on 5/1/2016.
 */
public class SocialContactActivity extends AppCompatActivity {

    private MainActivity MainObject;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
    }


    void intentMessageTelegram(String msg) {
        final String appName = "org.telegram.messenger";
        final boolean isAppInstalled = isAppAvailable(this, appName);
        if (isAppInstalled) {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            myIntent.setData(Uri.parse("http://telegram.me/35406 "));
            myIntent.setPackage(appName);
            myIntent.putExtra(Intent.EXTRA_TEXT, msg);//
            this.startActivity(Intent.createChooser(myIntent, "Share with"));
        } else {
            Toast.makeText(this, "Telegram not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isAppAvailable(Context context , String appName) {
        PackageManager pm = context.getPackageManager();
        try{

            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            /**
             * @return True if app is installed
             */
        return true;
    }catch (Exception e) {
        return false;
        }
    }
}

//references
//http://stackoverflow.com/questions/21627167/how-to-send-a-intent-with-telegram

