package com.example.mahdieh.dtgroup.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.inputmethodservice.Keyboard;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.Model.PolicyItem;
import com.example.mahdieh.dtgroup.R;

import java.util.Objects;
import java.util.Vector;

/**
 * Created by mahdieh on 5/3/2016.
 */
public class PolicyViewAdapter extends ArrayAdapter<PolicyItem> {

    private Context context;
    private Vector <PolicyItem> policyItems;
    private int resourceId;
    private Typeface tf, tfIcon;


    public PolicyViewAdapter(Context context, int resourceId, Vector<PolicyItem> objects, Typeface tf, Typeface tfIcon, Object o) {

        super(context, resourceId, objects);
        this.context=context;
        policyItems=objects;
        this.resourceId = resourceId;
        this.tf=tf;
        this.tfIcon = tfIcon;
    }


    public View getView (int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(resourceId, parent, false);

        PolicyItem policyItem = policyItems.get(position);
        TextView textView = (TextView) rowView.findViewById(R.id.policyItemParentTextView);
        textView.setText(policyItem.category);
        textView.setTypeface(tf);

        LinearLayout linearLayout = (LinearLayout) rowView.findViewById(R.id.policyItemParentLinearLayout);
        //add view per policyItem
        for (int i = 0; i< policyItem.content_policy.length; i++) {
            linearLayout.addView(getChildView(i, parent, policyItem));
        }

        return rowView;
    }

    private View getChildView(int position, ViewGroup parent, PolicyItem policyItem) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //assign rowView to item.xml & parent.xml

        View rowView = inflater.inflate(R.layout.activity_policy_row , parent, false);
        final TextView textViewPolicy = (TextView) rowView.findViewById(R.id.policyItemChildTextViewRule);
        textViewPolicy.setText(policyItem.content_policy[position]);
        textViewPolicy.setTypeface(tf);

        String [] nums = context.getResources().getStringArray(R.array.persianNums);
        final TextView textViewNum = (TextView) rowView.findViewById(R.id.PolicyItemChildNumber);
        textViewNum.setText(nums[position+1]);
        textViewNum.setTypeface(tf);

        return rowView;

    }


}
