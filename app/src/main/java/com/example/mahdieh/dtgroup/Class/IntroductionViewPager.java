package com.example.mahdieh.dtgroup.Class;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.mahdieh.dtgroup.Adapter.IntroViewPagerAdapter;
import com.example.mahdieh.dtgroup.Other.DataStorage;
import com.example.mahdieh.dtgroup.Other.Utils;
import com.example.mahdieh.dtgroup.R;
import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.views.ButtonRectangle;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

/**
 * Created by mahdieh on 5/3/2016.
 */
public class IntroductionViewPager extends FragmentActivity {

    private IntroViewPagerAdapter mAdapter;
    private ViewPager mPager;
    private PageIndicator mIndicator;
    private int currentItem;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_viewpager);
        
        mAdapter= new IntroViewPagerAdapter(getSupportFragmentManager(),this );
        mPager.setAdapter(mAdapter);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.introViewPagerActivityIndicator);
        mIndicator = indicator;
        indicator.setViewPager(mPager);
        indicator.setSnap(true);
        
        //----------------------
        currentItem = DataStorage.introductionPageCount -1;
        mPager.setCurrentItem(currentItem);
        mIndicator.setCurrentItem(currentItem);
        //-----------------------
        
        findViewById(R.id.introPagerActivityRightButton).setVisibility(View.VISIBLE);
        //--------------------------
        
        setTypefaceForAllViews();

        setAllOnClickListener();

    }

    private void setAllOnClickListener() {
        findViewById(R.id.introPagerActivityButtonContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(IntroductionViewPager.this, SearchActivity.class));
            }
        });

        findViewById(R.id.introPagerActivityLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPager.setCurrentItem(--currentItem);
            }
        });
        findViewById(R.id.introPagerActivityRightButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(++currentItem);
            }
        });
    }

    private void setTypefaceForAllViews() {

        Typeface tfIcon = Typeface.createFromAsset(getAssets(), "fonts/flaticon.ttf");
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile.ttf");

        //------------------
        //setTypeface to textview

        ((ButtonFlat)findViewById(R.id.introPagerActivityLeftButton)).getTextView().setTextSize(Utils.pixelsToSp(getResources().getDimension(R.dimen.textSizeLarge), getResources()));
        ((ButtonFlat)findViewById(R.id.introPagerActivityLeftButton)).getTextView().setTypeface(tfIcon);
        ((ButtonFlat)findViewById(R.id.introPagerActivityRightButton)).getTextView().setTextSize(Utils.pixelsToSp(getResources().getDimension(R.dimen.textSizeLarge), getResources()));
        ((ButtonFlat)findViewById(R.id.introPagerActivityRightButton)).getTextView().setTypeface(tfIcon);

        //--------------------------

        ((ButtonRectangle)findViewById(R.id.introPagerActivityButtonContinue)).getTextView().setTextColor(getResources().getColor(R.color.back_pink));
        ((ButtonRectangle)findViewById(R.id.introPagerActivityButtonContinue)).getTextView().setTypeface(tf);

        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                findViewById(R.id.introPagerActivityRightButton).setVisibility(View.VISIBLE);
                findViewById(R.id.introPagerActivityLeftButton).setVisibility(View.VISIBLE);

                if (position == 0){
                    ((ButtonRectangle) findViewById(R.id.introPagerActivityButtonContinue)).getTextView().setText(getString(R.string.start));
                    findViewById(R.id.introPagerActivityLeftButton).setVisibility(View.INVISIBLE);


                }else


                    ((ButtonRectangle) findViewById(R.id.introPagerActivityButtonContinue)).getTextView().setText(getString(R.string.skip));

                if (position == DataStorage.introductionPageCount -1)
                    findViewById(R.id.introPagerActivityRightButton).setVisibility(View.INVISIBLE);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


}
