package com.example.mahdieh.dtgroup.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.R;

/**
 * Created by mahdieh on 5/2/2016.
 */
public class SpinnerAdapter extends ArrayAdapter<String> {

    private Context context;
    String [] data = null;

    public SpinnerAdapter(AdapterView.OnItemSelectedListener context, int resource , String[] data) {
        super((Context) context, resource, data);
        this.context = (Context) context;
        this.data = data;
    }

    public View getDropDownView (int position, View ConvertView, ViewGroup parent) {
        View row = ConvertView;
        if (row == null){
            //inflate layout for the textView
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.spinner_item_row, parent, false);

        }


        //put data in it
        String item = data[position];
        if (item != null){
            TextView textView = (TextView) row.findViewById(R.id.searchActivitySpinnerTextViewRowItem);
            textView.setText(item);
        }
        return row;
    }
}
