package com.example.mahdieh.dtgroup.Model;

/**
 * Created by mahdieh on 5/25/2016.
 */
public class Flight {
    String price;
    int type;
    String time;
    int capacity;
    String num_flight;
    String airline;
    String Carrier;
    String expid;
    String id_flight;
    String RDB;
    String Link;
    String week;
    String startTripCity;
    String DestTripCity;


    public Flight(String price, int type, String time, int capacity, String num_flight,
                  String airline, String carrier, String expid, String id_flight,
                  String RDB, String link, String week, String startTripCity, String destTripCity) {
        this.price = price;
        this.type = type;
        this.time = time;
        this.capacity = capacity;
        this.num_flight = num_flight;
        this.airline = airline;
        Carrier = carrier;
        this.expid = expid;
        this.id_flight = id_flight;
        this.RDB = RDB;
        Link = link;
        this.week = week;
        this.startTripCity = startTripCity;
        DestTripCity = destTripCity;
    }

    public Flight(){
        //empty constructor
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getNum_flight() {
        return num_flight;
    }

    public void setNum_flight(String num_flight) {
        this.num_flight = num_flight;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getCarrier() {
        return Carrier;
    }

    public void setCarrier(String carrier) {
        Carrier = carrier;
    }

    public String getExpid() {
        return expid;
    }

    public void setExpid(String expid) {
        this.expid = expid;
    }

    public String getId_flight() {
        return id_flight;
    }

    public void setId_flight(String id_flight) {
        this.id_flight = id_flight;
    }

    public String getRDB() {
        return RDB;
    }

    public void setRDB(String RDB) {
        this.RDB = RDB;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getStartTripCity() {
        return startTripCity;
    }

    public void setStartTripCity(String startTripCity) {
        this.startTripCity = startTripCity;
    }

    public String getDestTripCity() {
        return DestTripCity;
    }

    public void setDestTripCity(String destTripCity) {
        DestTripCity = destTripCity;
    }
}