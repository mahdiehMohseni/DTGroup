package com.example.mahdieh.dtgroup.Model;

/**
 * Created by mahdieh on 5/2/2016.
 */
public class DrawerItem {
    private int icon;
    private String title;

    public DrawerItem(int resourceId, String s) {

    }

    public int getIcon(){
        return icon;
    }
    public void setIcon(int icon){
        this.icon=icon;
    }

    public String getTitle(){

        return title;
    }

    public void setTitle(String title){
        this.title=title;
    }
}
