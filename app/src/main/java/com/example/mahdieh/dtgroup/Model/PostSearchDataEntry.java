package com.example.mahdieh.dtgroup.Model;

import com.google.gson.annotations.SerializedName;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import java.util.Date;
import java.util.List;

/**
 * Created by mahdieh on 5/14/2016.
 */
public class PostSearchDataEntry {

    @SerializedName("id")
    public long ID;
    public String destinationSpinnerSelected;
    public String spinner_adultSelected;
    public String spinner_KidsSelected;
    public String spinner_BabySelected;
    public String url;
    @SerializedName("date")
    public Date dateCreated;
    public DatePickerDialog day, month, year;
    public String body;

    public List tags;
    public PostSearchDataEntry () {

    }

}

//Tag.java
class Tag {

    public String name;
    public String url;

    public Tag() {

    }
}
