package com.example.mahdieh.dtgroup.Class;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mahdieh.dtgroup.Adapter.ResultActivityAdapter;
import com.example.mahdieh.dtgroup.Model.FlightItem;
import com.example.mahdieh.dtgroup.Model.PostSearchDataEntry;
import com.example.mahdieh.dtgroup.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.msgpack.util.json.JSON;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.xml.transform.Result;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by mahdieh on 5/2/2016.
 */
public class ResultActivity extends AppCompatActivity {


//    // All static variables
//    static final String URL = "this.is.a.url";
//    // XML node keys
//    static final String KEY_DATA = "data"; // parent node
//    static final String KEY_ID = "id";
//    static final String KEY_TITLE = "title";
//    static final String KEY_DATE1 = "date1";
//    static final String KEY_DATE2 = "date2";
//    static final String KEY_DATE3 = "date3";
//    static final String KEY_DATE2VIS = "date2vis";
//    static final String KEY_DATE3VIS = "date3vis";
//    static final String KEY_PLATFORMS = "platforms";
//    static final String KEY_THUMB_URL = "thumb_url";
//
//
//    ArrayList<HashMap<String, String>> songsList;

    RecyclerView recyclerView;
    ArrayList<FlightItem> flightItemsList;
    private View resultView;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;

    private int fullCapacity, AvailableCapacity, limitedCapacity;
    private int mDatasetTypes[] = {AvailableCapacity, limitedCapacity, fullCapacity}; //view types
    private static final String TAG = "POST";
    private ResultActivityAdapter adapter;


    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private String url = "http://charter724.ir/webservice/get_city_b.php?id=213&key=3nN1iR74IK0Vokr81oJZ16r3l&from=mhd"; // url ????

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        String TAG = ResultActivity.class.getSimpleName();
        //setupToolbar();

        // ===================== //
        final TextView view = (TextView) findViewById(R.id.resultActivityTextViewRibbon);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResultActivity.this, FlightInfoActivity.class));
            }

        });
        // =========================  sending jsonRequest ======================= //
        sendJsonRequest();
        //=============== Initialize the RecyclerView ===================//
        recyclerView = (RecyclerView) findViewById(R.id.resultActivityRecyclerView);
        recyclerView.setHasFixedSize(true);


        mLayoutManager = new LinearLayoutManager(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ResultActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());


        //=============== Initialize the Adapter, Data Array and set Adapter ===================//
        flightItemsList = new ArrayList<>();
        //ResultActivityAdapter adapter = new ResultActivityAdapter(this, flightItemsList);
        //recyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

    }

    private void sendJsonRequest() {
        Bundle extras = getIntent().getExtras();
        if(extras!=null)
        {

            Boolean checkBox1 = getIntent().getExtras().getBoolean("checkBox1Value");
            String StrOfCheckBox1 = String.valueOf(checkBox1);
            Boolean checkBox2 = getIntent().getExtras().getBoolean("checkBox2Value");
            String StrOfCheckBox2= String.valueOf(checkBox2);
            String StartSpinnerResult = extras.getString("StartSpinnerSelected");
            String DestinationSpinnerResult = extras.getString("destinationSpinnerSelected");
            String spinner_adultResult = extras.getString("spinner_adultSelected");
            String spinner_KidsResult = extras.getString("spinner_KidsSelected");
            String spinner_BabyResult = extras.getString("spinner_BabySelected");
            int daySelected = extras.getInt("day");
            String strOfdaySelected = String.valueOf(daySelected);
            int monthSelected = extras.getInt("month");
            String strOfmonthSelected = String.valueOf(monthSelected);
            int yearSelected = extras.getInt("year");
            String strOfyearSelected = String.valueOf(yearSelected);

            //------------------------
            ArrayList<String> StrPostResult = new ArrayList<String>();
            StrPostResult.add(StartSpinnerResult);
            StrPostResult.add(DestinationSpinnerResult); //this adds an element to the list.
            StrPostResult.add(spinner_adultResult); //this adds an element to the list.
            StrPostResult.add(spinner_KidsResult); //this adds an element to the list.
            StrPostResult.add(spinner_BabyResult); //this adds an element to the list.
            StrPostResult.add(strOfdaySelected); //this adds an element to the list.
            StrPostResult.add(strOfmonthSelected); //this adds an element to the list.
            StrPostResult.add(strOfyearSelected); //this adds an element to the list.
            StrPostResult.add(StrOfCheckBox1);
            StrPostResult.add(StrOfCheckBox2);
            //------------------------
            OkHttpClient client = new OkHttpClient();
            RequestBody Result_Post_body = new FormBody.Builder()
                    .add("StrPostResult[0]", "StartSpinnerSelected")
                    .add("StrPostResult[1]", "destinationSpinnerSelected")
                    .add("StrPostResult[2]", "spinner_adultSelected")
                    .add("StrPostResult[3]", "spinner_KidsSelected")
                    .add("StrPostResult[4]", "spinner_BabySelected")
                    .add("StrPostResult[5]", "day")
                    .add("StrPostResult[6]", "month")
                    .add("StrPostResult[7]", "year")
                    .add("StrPostResult[8]", "checkBox1Value" )
                    .add("StrPostResult[9]", "checkBox2Value" )
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(Result_Post_body)
                    .build();
            try {
                Response ResultResponse = client.newCall(request).execute();
                if (!ResultResponse.isSuccessful())
                    throw new IOException("Unexpected code " + ResultResponse);
                Log.d("Response:", ResultResponse.body().string());

                // ===================== //
                final String JsonData = ResultResponse.body().string(); //Get API response
                ResultActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject jsonObject = null;
                        try {
                            //parse json
                            jsonObject = new JSONObject(JsonData);
                            //get array
                            JSONArray jsonArray = jsonObject.getJSONArray("jsonArrayResult");
                            //loop through array
                            FlightItem flightResult = null;
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject item = jsonArray.getJSONObject(i);
                                //-----------------------------

                                flightResult = new FlightItem(resultView);
                                flightResult.setAgencyName("AgencyName");
                                flightResult.setDayOfFlight("DayOfFlight");
                                flightResult.setDestinationTripCity("DestinationTripCity");
                                flightResult.setStartTime("StartTime");
                                flightResult.setStartTripCity("StartTripCity");
                                flightResult.setEndTime("EndTime");
                                flightResult.setRequestType("RequestType");
                                flightResult.setAgencyCode(Integer.parseInt("agencyCode")); //????
                                flightResult.setAgencylogo("AgencyLogo");
                                flightResult.setPrice(Long.valueOf("Price")); //?????

                                /*
                                JSONArray jsa = item.getJSONArray("flights");
                                for (int m=0; m<jsa.length(); m++) {
                                    JSONObject thumbnail = jsa.getJSONObject(0);
                                    flightResult.setLogo(thumbnail.getString("url"));
                                 */
                            }
                            flightItemsList.add(flightResult);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            } catch (IOException e) {
                e.printStackTrace();
            }
            // ===================== //
        }
        else
        {

            TextView txtAlert = (TextView) findViewById(R.id.resultActivityTextViewNoflightAlert);
            txtAlert.setText(getResources().getString(R.string.noFlightAlert));
        }

    }

    //==============================================//
//    private void setupToolbar() {
//
//         Toolbar ResultActivityToolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(ResultActivityToolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.previous);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
////        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
////        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.previous));
////        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                startActivity(new Intent(ResultActivity.this, SearchActivity.class));
////            }
////        });
//    }


     //for back arrow

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), SearchActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }


}

/*
//references
http://stackoverflow.com/questions/26651602/display-back-arrow-on-toolbar-android
http://stackoverflow.com/questions/36347083/using-okhttp-to-get-a-json-file-but-need-to-add-a-username-and-password
http://stackoverflow.com/questions/33307395/how-to-add-array-to-okhttp-body-post
http://stackoverflow.com/questions/31572294/the-simplest-way-to-display-strings-with-icons-in-a-recyclerview

 */








