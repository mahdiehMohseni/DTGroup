package com.example.mahdieh.dtgroup.Model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.Locale;

/**
 * Created by mahdieh on 5/11/2016.
 */
public class FlightItem {
    private View resultView;
    private String startTime;
    private String endTime;
    //private String Image;
    private String agencyName;
    private int limitedCapacity;
    private int FullCapacity;
    private String date_Flight;
    private String startTripCity;
    private String destinationTripCity;
    private String agencylogo;
    private Long price;
    private int agencyCode;
    private String requestType;
    private String capacityStatus;

    public FlightItem(String startTime, int limitedCapacity, int FullCapacity, String endTime, String agencyName, String dayOfFlight, String startTripCity, String destinationTripCity, String agencylogo, Long price, int agencyCode, String requestType, String capacityStatus) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.agencyName = agencyName;
        this.limitedCapacity = limitedCapacity;
        this.FullCapacity= FullCapacity;
        this.date_Flight = dayOfFlight;
        this.startTripCity = startTripCity;
        this.destinationTripCity = destinationTripCity;
        this.agencylogo = agencylogo;
        this.price = price;
        this.agencyCode = agencyCode;
        this.requestType = requestType;
        this.capacityStatus = capacityStatus;
    }

    public FlightItem(View resultView) {
    }


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getlimitedCapacity(int limitedCapacity){ return limitedCapacity; }
    public void setlimitedCapacity (int limitedCapacity) { this.limitedCapacity = limitedCapacity; }

    public int getFullCapacity(int FullCapacity){ return FullCapacity; }
    public void setFullCapacity(int FullCapacity){ this.FullCapacity = FullCapacity; }


    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getDayOfFlight() {
        return date_Flight;
    }

    public void setDayOfFlight(String dayOfFlight) {
        this.date_Flight = dayOfFlight;
    }

    public String getStartTripCity() {
        return startTripCity;
    }

    public void setStartTripCity(String startTripCity) {
        this.startTripCity = startTripCity;
    }

    public String getDestinationTripCity() {
        return destinationTripCity;
    }


    public void setDestinationTripCity(String destinationTripCity) {
        this.destinationTripCity = destinationTripCity;
    }

    public String getAgencylogo() {
        return agencylogo;
    }

    public void setAgencylogo(String agencylogo) {
        this.agencylogo = agencylogo;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public int getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(int agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getCapacityStatus() {
        return capacityStatus;
    }

    public void setCapacityStatus(String capacityStatus) {
        this.capacityStatus = capacityStatus;
    }



}
