package com.example.mahdieh.dtgroup.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.Class.IntroductionActivity;
import com.example.mahdieh.dtgroup.Class.PolicyActivity;
import com.example.mahdieh.dtgroup.Model.IntroItem;
import com.example.mahdieh.dtgroup.Model.PolicyItem;
import com.example.mahdieh.dtgroup.R;

import java.util.Objects;
import java.util.Vector;

/**
 * Created by mahdieh on 5/1/2016.
 */
public class ListViewAdapter extends ArrayAdapter <IntroItem> {

    private Context context;
    private Vector<IntroItem> introItems;
    private int resourceId;
    private Typeface tf, tfIcon;

    public  ListViewAdapter(Context context , int resourceId, Vector<IntroItem> objects, Typeface tf, Typeface tfIcon, Object o){
        super(context, resourceId, objects);
        this.context = context;
        this.resourceId = resourceId;
        introItems= objects;
        this.tf=tf;
        this.tfIcon = tfIcon;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(resourceId, parent, false);
        IntroItem introItem = introItems.get(position);
        TextView textView = (TextView) rowView.findViewById(R.id.IntroItemParentTextView);
        textView.setText(introItem.category);
        textView.setTypeface(tf);


        LinearLayout linearLayout = (LinearLayout) rowView.findViewById(R.id.IntroItemParentLinearLayout);
        //add view per introItem
        for (int i=0; i<introItem.intro_content.length; i++) {
            linearLayout.addView(getChildView (i, parent, introItem));
        }
        return rowView;

    }

    private View getChildView(int position, ViewGroup parent, IntroItem introItem) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //assign rowview to items and parent
        View rowView = inflater.inflate(R.layout.intro_item, parent, false);

        final TextView textViewIntro = (TextView) rowView.findViewById(R.id.introItemChildTextViewIntro);
        textViewIntro.setText(introItem.intro_content[position]);
        textViewIntro.setTypeface(tf);

        return rowView;
    }


}
