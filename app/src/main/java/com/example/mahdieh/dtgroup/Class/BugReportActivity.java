package com.example.mahdieh.dtgroup.Class;

/**
 * Created by mahdieh on 5/1/2016.
 */
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.OnClick;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.example.mahdieh.dtgroup.R;


import java.io.IOException;


public class BugReportActivity extends AppCompatActivity {
    //==============url ??????? ========
    private static final String BASE_URL = "http://10.0.2.2/tcdc/check2.php";
 //===================?????

    @Bind(R.id.bugReportActivityEditTextName)
    EditText userNameTextEdit;
    @Bind(R.id.bugReportActivityEditTextEmail)
    EditText emailTextEdit;
    @Bind(R.id.bugReportActivityEditTextMessage)
    EditText messageTextEdit;
//    @Bind(R.id.bugReportActivityButtonSend)
//    Button sendBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bug_report);
     }

    //============= @OnClick====================
    @OnClick(R.id.bugReportActivityButtonSend)
    public void onClick(View view){
        String UserName = userNameTextEdit.getText().toString();
        String Email = emailTextEdit.getText().toString();
        String msg = messageTextEdit.getText().toString();
        bugReportUser(UserName, Email, msg);
    }

    //================bugReportUser===================================
    public void bugReportUser(String UserName, String Email, String msg) {
        RequestBody body = new FormBody.Builder()
                .add("username", UserName)
                .add("email", Email)
                .add("message", msg)
                .build();

        Request request = new Request.Builder().url(BASE_URL).post(body).build();
        OkHttpClient client = new OkHttpClient();


        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                //  Log.e(TAG_REGISTER, "Registration error: " + e.getMessage());
                System.out.println("bug report Error" + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                try {

                    String resp = response.body().string();
//                    Log.v(TAG_REGISTER, resp);
                    System.out.println(resp);
                    if (response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "your message send successful...thank you!",
                                Toast.LENGTH_LONG).show();


                    } else {

                        Toast.makeText(getApplicationContext(), getString(R.string.errorConnectNet),
                                Toast.LENGTH_LONG).show();

                    }

                } catch (IOException e) {

                    // Log.e(TAG_REGISTER, "Exception caught: ", e);
                    System.out.println("Exception caught" + e.getMessage());
                }
            }
        });
    }

    //================onCreateOptionsMenu=========================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //==================onOptionsItemSelected===================
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}




// http://stackoverflow.com/questions/32856401/using-okhttp-library-for-posting-to-a-php-script-that-saves-to-mysql
