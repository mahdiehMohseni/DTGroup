package com.example.mahdieh.dtgroup.Class;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mahdieh.dtgroup.Model.PlaneItem;
import com.example.mahdieh.dtgroup.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * Created by mahdieh on 5/2/2016.
 */
public class SearchActivity extends MainActivity implements RecyclerView.OnItemTouchListener {

    AdapterView.OnItemSelectedListener myListener;
    private Button searchActivityDate;
    // variables to save user selected date and time
    public  int yearSelected ,monthSelected,daySelected;
    private Spinner kidsSpinner, babySpinner, adultSpinner;
    ArrayList<String> checkBox_selection = new ArrayList<String>();
    private String spinner_adultSelected, spinner_KidsSelected, spinner_BabySelected,StartSpinnerSelected, destinationSpinnerSelected;

    // declare  the variables to show the date and time whenTime and Date Picker Dialog first appears
    private int mYear, mMonth, mDay;
    private String mMessage= "onFailure: ";
    private static final String LOG_TAG = "OkHttp";
    private DatePickerDialog datePickerDialog;
    private PersianCalendar persianCalendar;
    private static final String DATEPICKER = "DatePickerDialog";


    // ---------------------------  urls

    // Url to create new category
    private String URL_SPINNER1 = "http://charter724.ir/webservice/list_flight.php?id=213&key=3nN1iR74IK0Vokr81oJZ16r3l&from=mhd&to=thr&date=2015-10-20";
    private String URL_SPINNER2 = "http://charter724.ir/webservice/list_flight.php?id=213&key=3nN1iR74IK0Vokr81oJZ16r3l&from=mhd&to=thr&date=2015-10-20";
    //-----array adapter
    private ArrayList<PlaneItem> planeItems;
    private ArrayAdapter<PlaneItem> adapter1,adapter2;
    private static final String TAG = "SpinnerLoadFromNetwork";
//--------------------------------
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        //populate data to static spinners
        addItemsOnKidsSpinner();
        addItemsOnBabySpinner();
        addItemsOnAdultSpinner();
        addItemOnStartTripSpinner();
        addItemOnDestinationTripSpinner();



        //--------- set Text To TextView items -------------
        TextView TimeTrip = (TextView) findViewById(R.id.searchActivityTextViewTimeOfTrip);
        TimeTrip.setText(getResources().getString(R.string.TimeCalendar));
        //TextView dateTextView = (TextView) findViewById(R.id.searchActivityDate1);
        //-----------------------
                TextView description = (TextView) findViewById(R.id.SearchActivityTextViewDescription);
        description.setText(getResources().getString(R.string.searchTitlePage));
        TextView StartTripText = (TextView) findViewById(R.id.searchActivityTextViewStartTrip);
        StartTripText.setText(getResources().getString(R.string.startTrip));
        //-----------------------------
        TextView DestinationTripText = (TextView) findViewById(R.id.searchActivityTextViewDestinationTrip);
        DestinationTripText.setText(getResources().getString(R.string.destinationTrip));
        //--------------------------------------
        TextView AdultTripText = (TextView) findViewById(R.id.searchActivityTextViewNumbersAdult);
        AdultTripText.setText(getResources().getString(R.string.adult));
        //------------------------------------
        TextView KidsTripText = (TextView) findViewById(R.id.searchActivityTextViewNumbersKids);
        KidsTripText.setText(getResources().getString(R.string.Kids));
        //------------------------
        TextView BabyTripText = (TextView) findViewById(R.id.searchActivityTextViewNumbersBaby);
        BabyTripText.setText(getResources().getString(R.string.baby));
        //----------------------
        TextView PolicyTripCancelling = (TextView) findViewById(R.id.searchActivityTextCheckBox1);
        PolicyTripCancelling.setText(getResources().getString(R.string.policyOfBookingforCancelling));
        //------------
        TextView PolicyTripDirect = (TextView) findViewById(R.id.searchActivityTextCheckBox2);
        PolicyTripDirect.setText(getResources().getString(R.string.policyOfBookingforDirectFlight));
         searchActivityDate = (Button) findViewById(R.id.searchActivityDate1);
        //searchActivityDate.setText(getResources().getString(R.string.selected_date));
        //--------------------------------
        //final Button dateButton = (Button) findViewById(R.id.btnDateOk);

        //--define spinners
        final Spinner DestinationTripSpinner = (Spinner) findViewById(R.id.spinner_show2);
        final Spinner adultSpinner = (Spinner) findViewById(R.id.spinner_adult);
        final Spinner kidsSpinner = (Spinner) findViewById(R.id.spinner_kids);
        final Spinner babySpinner = (Spinner) findViewById(R.id.spinner_baby);
        final Spinner StartTripSpinner = (Spinner) findViewById(R.id.spinner_show1);

//---------------------your ItemSelectedListener needs to be registered in the Spinner
        adultSpinner.setOnItemSelectedListener(myListener);
        kidsSpinner.setOnItemSelectedListener(myListener);
        babySpinner.setOnItemSelectedListener(myListener);
        StartTripSpinner.setOnItemSelectedListener(myListener);
        DestinationTripSpinner.setOnItemSelectedListener(myListener);
    //searchActivityDate.setOnClickListener((View.OnClickListener) myListener);
         //searchActivityDate.setOnItemSelectedListener(myListener);

//----------------------
        //--define checkBox
        final CheckBox checkBox2 = (CheckBox) findViewById(R.id.searchActivityCheckBox2);
        final CheckBox checkBox1 = (CheckBox) findViewById(R.id.searchActivityCheckBox1);
        //search_Btn
        Button searchButton = (Button) findViewById(R.id.searchActivityButton);
        // ---------------------------- ?????????
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                    String Spinner2Value = DestinationTripSpinner.getSelectedItem().toString();
                    String Spinner1Value = StartTripSpinner.getSelectedItem().toString();
                    if (Spinner2Value == Spinner1Value){
                        AlertDialog alertDialog = new AlertDialog.Builder(
                                SearchActivity.this).create();
                        // Setting Dialog Title
                        alertDialog.setTitle(getResources().getString(R.string.alertTitle));
                        // Setting Dialog Message
                        alertDialog.setMessage(getResources().getString(R.string.alertEqual));
                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.warning);
                        // Setting OK Button
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog closed
                                Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                            }
                        });
                        // Showing Alert Message
                        alertDialog.show();

                } else {

                        Integer adultNumValue =  (Integer) adultSpinner.getSelectedItem();
                        Integer kidsNumValue = (Integer) kidsSpinner.getSelectedItem();
                        Integer SumResult = adultNumValue + kidsNumValue; //Result
                        if (SumResult > 9 ){

                            AlertDialog alertDialog = new AlertDialog.Builder(
                                    SearchActivity.this).create();
                            alertDialog.setTitle(getResources().getString(R.string.alertTitle));
                            alertDialog.setMessage(getResources().getString(R.string.sumofKidsAndAdultValue));
                            alertDialog.setIcon(R.drawable.warning);
                            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                                }
                            });
                            alertDialog.show();
                        }else {
                            Integer adultValue =  (Integer) adultSpinner.getSelectedItem();
                            Integer babyValue = (Integer) babySpinner.getSelectedItem();
                            if (babyValue > adultValue) {
                                AlertDialog alertDialog = new AlertDialog.Builder(
                                        SearchActivity.this).create();
                                alertDialog.setTitle(getResources().getString(R.string.alertTitle));
                                alertDialog.setMessage(getResources().getString(R.string.alertOnBabyAndAdultValue));
                                alertDialog.setIcon(R.drawable.warning);
                                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                alertDialog.show();

                            } else {
                    Intent intent = new Intent(SearchActivity.this, NewResultActivity.class);
                    intent.putExtra("StartSpinnerSelected",StartSpinnerSelected );//StartTripSpinner.getSelectedItem().toString()
                    intent.putExtra("destinationSpinnerSelected", destinationSpinnerSelected);//DestinationTripSpinner.getSelectedItem().toString()
                    intent.putExtra("spinner_adultSelected", spinner_adultSelected);//adultSpinner.getSelectedItem().toString()
                    intent.putExtra("spinner_KidsSelected", spinner_KidsSelected);//kidsSpinner.getSelectedItem().toString()
                    intent.putExtra("spinner_BabySelected", spinner_BabySelected); //babySpinner.getSelectedItem().toString()
                                //for image
                    intent.putExtra("day", daySelected);
                    intent.putExtra("month", monthSelected);
                    intent.putExtra("year", yearSelected);
                    intent.putExtra("checkBox1Value", checkBox1.isChecked());
                    intent.putExtra("checkBox2Value", checkBox2.isChecked());

//                                intent.putExtra("CheckBox_Selection", final_text.getText().toStrng());
//
//                                Intent intent = new Intent(topPawikan.this, SecondActivity.class);
//                                Bundle b = new Bundle();
//                                b.putString("selection", final_text.getText().toString());
//                                intent.putExtras(b);


                                startActivity(intent);
                    finish();

                            }
                        }
                }
            }

        });
     //--------------------- switch case between spinners

        AdapterView.OnItemSelectedListener myListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                switch (parent.getId()) {
                    case R.id.spinner_show1:
                        //------------------
                        StartSpinnerSelected = parent.getItemAtPosition(position).toString();
                        break;
                    //------------------------------------------------------------------------------------
                    case R.id.spinner_show2:
                        destinationSpinnerSelected = parent.getItemAtPosition(position).toString();
                        break;
                    //-------------------------------------------------------------------------------------------
                    case R.id.spinner_adult:
                        spinner_adultSelected = parent.getItemAtPosition(position).toString();

                        break;
                    //-----------------------------------------------------------------------------------------------
                    case R.id.searchActivityDate1: {

                       searchActivityDate = (Button) findViewById(R.id.searchActivityDate1);
                        searchActivityDate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final PersianCalendar myCal = new PersianCalendar();
                                // Use the current date as the default date in the picker
                                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                                        (DatePickerDialog.OnDateSetListener) SearchActivity.this, mYear=myCal.getPersianYear(),
                                        mMonth=myCal.getPersianMonth(),
                                        mDay=myCal.getPersianDay()
                                );
                                datePickerDialog.show(getFragmentManager(), DATEPICKER );

                                //Update Calendar by USER'S VALUES
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                        myCal.set(PersianCalendar.YEAR, year);
                                        myCal.set(PersianCalendar.MONTH, monthOfYear);
                                        myCal.set(PersianCalendar.DAY_OF_MONTH, dayOfMonth);

                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                searchActivityDate.setText(date);
                                searchActivityDate.setTypeface(tf);
                                //--------------------
                                yearSelected = myCal.get(year);
                                monthSelected=myCal.get(monthOfYear);
                                daySelected=myCal.get(dayOfMonth);
                            }
                        };
                            }
                        });
                        break;
                    }
                    //----------------------------------------------------------------------------------------------------
                    case R.id.spinner_kids:
                        spinner_KidsSelected = parent.getItemAtPosition(position).toString();
                        break;
                    //----------------------------------------------------------------------------------------------------
                    case R.id.spinner_baby:
                        spinner_BabySelected = parent.getItemAtPosition(position).toString();
                        break;
                    //----------------------------
                    case R.id.searchActivityCheckBox1:

                        boolean checked = ((CheckBox) view).isChecked();
                        if (checked)
                        {checkBox_selection.add("Maglibot");}

                        /**
                         * checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton view, boolean isChecked) {

                        //init
                        SharedPreferences prefs = getSharedPreferences("checkBox1Status", 0);
                        SharedPreferences.Editor editor = prefs.edit();

                        // Save
                        Boolean checkBox1Value = checkBox1.isChecked();
                        editor.putBoolean("checkBox1Value", checkBox1Value);
                        editor.commit();

                        //load
                        checkBox1.setChecked(prefs.getBoolean("checkBox1Value", false));
                        }
                        });
                         */
                        break;
                    //---------------
                    case R.id.searchActivityCheckBox2:

                        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton view, boolean isChecked) {

                                //init
                                SharedPreferences prefs = getSharedPreferences("checkBox2Status", 0);
                                SharedPreferences.Editor editor = prefs.edit();

                                // Save
                                Boolean checkBox1Value = checkBox2.isChecked();
                                editor.putBoolean("checkBox2Value", checkBox1Value);
                                editor.commit();

                                //load
                                checkBox2.setChecked(prefs.getBoolean("checkBox2Value", false));
                            }
                        });
                        break;
                    default:
                        break;
               }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }
    //--------------addItemOnDestinationTripSpinner
    private void addItemOnDestinationTripSpinner() {

        OkHttpClient client2 = new OkHttpClient();
        Request request2 = new Request.Builder()
                .url(URL_SPINNER2)
                .build();

        Response response2 = null;
        // Get a handler that can be used to post to the main thread

        Call call = client2.newCall(request2);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Log.d(TAG, "onFailure:");
               /*
                mMessage = e.toString();
                Log.e(LOG_TAG, mMessage); // no need inside run()
                */

            }

            @Override
            public void onResponse(Call call, Response response2) throws IOException {
                try {
                    //read data on the worker thread
                    final String responseData2 = response2.body().string();
                    //Getting JSON Object
                    JSONObject jsonObject = new JSONObject(responseData2);
                    JSONArray jsonArray2 = jsonObject.getJSONArray("result");
                    Type type2 = new TypeToken<ArrayList<PlaneItem>>() {
                    }.getType();

                    planeItems = (new Gson()).fromJson(jsonArray2.toString(), type2);
                    adapter2 = new ArrayAdapter<PlaneItem>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, planeItems);//????????
                    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//????????????????

                    SearchActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final Spinner DestinationTripSpinner = (Spinner) findViewById(R.id.spinner_show2);
                            DestinationTripSpinner.setAdapter(adapter2);
                            TextView mTextView = (TextView) findViewById(R.id.searchActivitySpinnerTextViewRowItem);
                            mTextView.setText(responseData2);
                            mTextView.setTypeface(tf);
                            //DestinationTripSpinner.setSelection(position);
                            //String Spinner2Value =  DestinationTripSpinner.getSelectedItem().toString();

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    //--------------addItemOnStartTripSpinner
    private void addItemOnStartTripSpinner() {

        OkHttpClient client1 = new OkHttpClient();
        Request request = new Request.Builder()
                .header("SpinnerStartTrip", "spinnerStartValues")
                .url(URL_SPINNER1)
                .build();
        // Get a handler that can be used to post to the main thread
        Call call = client1.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //alertUserAboutError();
                mMessage = e.toString();
                Log.e(LOG_TAG, mMessage); // no need inside run()
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    //read data on the worker thread
                    final String responseData = response.body().string();
                    JSONObject jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    Type type = new TypeToken<ArrayList<String>>() {
                    }.getType();

                    planeItems = (new Gson()).fromJson(jsonArray.toString(), type);
                    adapter1 = new ArrayAdapter<PlaneItem>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, planeItems);//?????????

                    //final ArrayAdapter<String> adapter1 = new ArrayAdapter (SearchActivity.this, R.layout.spinner_item_row, planeItems[0]);
                    adapter1.setDropDownViewResource(R.layout.spinner_dropdown_item);//or support_simple_spinner_dropdown_item
                    SearchActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final Spinner StartTripSpinner = (Spinner) findViewById(R.id.spinner_show1);
                            StartTripSpinner.setAdapter(adapter1);
                            TextView mTextView = (TextView) findViewById(R.id.searchActivitySpinnerTextViewRowItem);
                            mTextView.setText(responseData);
                            mTextView.setTypeface(tf);
                            //StartTripSpinner.setSelection(position);
                            //String Spinner1Value =  StartTripSpinner.getSelectedItem().toString();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    //--------------------addItemsOnAdultSpinner

    private void addItemsOnAdultSpinner() {

        adultSpinner = (Spinner) findViewById(R.id.spinner_adult);
        List<String> adult_list = new ArrayList<String>();
        adult_list.add("item 1");
        adult_list.add("item 2");
        adult_list.add("item 3");
        adult_list.add("item 4");
        adult_list.add("item 5");
        adult_list.add("item 6");
        adult_list.add("item 7");
        adult_list.add("item 8");
        adult_list.add("item 9");
        ArrayAdapter<String> AdultAdapter = new ArrayAdapter<String>(SearchActivity.this,
                android.R.layout.simple_spinner_item, adult_list);
        AdultAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//????????????????
        adultSpinner.setAdapter(AdultAdapter);
    }

    private void addItemsOnBabySpinner() {
        babySpinner = (Spinner) findViewById(R.id.spinner_baby);
        List<String> baby_list = new ArrayList<String>();
        baby_list.add("item 1");
        baby_list.add("item 2");
        baby_list.add("item 3");
        baby_list.add("item 4");
        baby_list.add("item 5");
        baby_list.add("item 6");
        baby_list.add("item 7");
        baby_list.add("item 8");
        baby_list.add("item 9");
        ArrayAdapter<String> BabyAdapter = new ArrayAdapter<String>(SearchActivity.this,
                android.R.layout.simple_spinner_item, baby_list); //????????????????/
        BabyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//???????????????????
        babySpinner.setAdapter(BabyAdapter);


    }

    private void addItemsOnKidsSpinner() {
         kidsSpinner = (Spinner) findViewById(R.id.spinner_kids);
        // add items into spinner dynamically
        List<String> Kids_list = new ArrayList<String>();
        Kids_list.add("item 1");
        Kids_list.add("item 2");
        Kids_list.add("item 3");
        Kids_list.add("item 4");
        Kids_list.add("item 5");
        Kids_list.add("item 6");
        Kids_list.add("item 7");
        Kids_list.add("item 8");
        Kids_list.add("item 9");
        ArrayAdapter<String> KidsAdapter = new ArrayAdapter<String>(SearchActivity.this,
                android.R.layout.simple_spinner_item, Kids_list);
        KidsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//??????????????/
        kidsSpinner.setAdapter(KidsAdapter);

    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    /*

       IMPLEMENTS , DatePickerDialog.OnDateSetListener

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

         // Note: monthOfYear is 0-indexed
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
        searchActivityDate.setText(date);
        searchActivityDate.setTypeface(tf);


    }
    */

}

//----------------------------------------------------------------------------------------------
/*
  references :
  http://stackoverflow.com/questions/13716251/how-to-implements-multiple-spinner-with-different-item-list-and-different-action
  http://stackoverflow.com/questions/18710666/creating-multiple-onitemselectedlisteners
  http://stackoverflow.com/questions/12792714/onitemselectedlistener-on-multiple-spinners-not-working
  //okhttp
  https://guides.codepath.com/android/Using-OkHttp
  http://www.learn-android-easily.com/2013/01/using-timepickerdialog-and.html
  http://stackoverflow.com/questions/6800244/android-how-to-save-the-state-of-a-checkbox
https://github.com/pankajnimgade/Tutorial/blob/master/app/src/main/java/spinner/list/activities/SpinnerLoadFromNetworkActivity.java
https://github.com/pankajnimgade/Tutorial/blob/master/app/src/main/java/spinner/list/activities/SpinnerLoadFromNetworkActivity.java
//http://www.scriptscoop.net/t/9d53aa8f3372/android-date-picker-from-calendar-popdown.html

*/

/*

/*
image.setImageResource(imgs.getResourceId(
						spinner.getSelectedItemPosition(), -1));
 */
                       /*
                        Integer []AdultNumitems = new Integer[]{1,2,3,4,5,6,7,8,9};
                        ArrayAdapter<Integer> adultAdapter = new ArrayAdapter<Integer>(SearchActivity.this, R.layout.spinner_item_row, AdultNumitems);
                        adultAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        adultSpinner.setAdapter(adultAdapter);
                        //adultSpinner.setSelection(position);
                        */

 /*
                                final PersianCalendar myCal = (PersianCalendar) PersianCalendar.getInstance();
                                // Use the current date as the default date in the picker
                                final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                                        SearchActivity.this,
                                        mYear= myCal.getPersianYear(),
                                        mMonth= myCal.getPersianMonth(),
                                        mDay= myCal.getPersianDay()
                                );




                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                        /*
                                        myCal.set(PersianCalendar.YEAR, year);
                                        myCal.set(PersianCalendar.MONTH, monthOfYear);
                                        myCal.set(PersianCalendar.DAY_OF_MONTH, dayOfMonth);


                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                searchActivityDate.setText(date);
                                searchActivityDate.setTypeface(tf);
                                //--------------------
                                yearSelected = myCal.get(year);
                                monthSelected=myCal.get(monthOfYear);
                                daySelected=myCal.get(dayOfMonth);
                            }
                        };
                                 */