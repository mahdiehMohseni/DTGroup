package com.example.mahdieh.dtgroup.Class;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.R;

/**
 * Created by mahdieh on 5/4/2016.
 */
public class IntroductionPagerFragment extends Fragment {
    private static final String KEY_DESCRIPTION = "IntroductionPagerFragment:Description";
    private static final String KEY_TItle = "IntroductionPagerFragment:Title";
    private static final String KEY_IMAGEID = "IntroductionPagerFragment:ImageId";

    private String mTitle;
    private String mDescription;
    private int mImageId;

    public static IntroductionPagerFragment newInstance (String title, String content, int imageId) {
        IntroductionPagerFragment fragment = new IntroductionPagerFragment();
        fragment.mDescription=content;
        fragment.mTitle=title;
        fragment.mImageId = imageId;

        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((savedInstanceState !=null)) {
            if (savedInstanceState.containsKey(KEY_DESCRIPTION))
                mDescription = savedInstanceState.getString(KEY_DESCRIPTION);
            if (savedInstanceState.containsKey(KEY_TItle))
                mTitle = savedInstanceState.getString(KEY_TItle);
            if (savedInstanceState.containsKey(KEY_IMAGEID))
                mImageId = savedInstanceState.getInt(KEY_IMAGEID);
        }
        }

    public View onCreateView(LayoutInflater mInflater, ViewGroup container, Bundle savedInstanceState) {
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANSansMobile.ttf");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View layout = inflater.inflate(R.layout.introduction_viewpager, null);

        ((TextView) layout.findViewById(R.id.introPagerTitleTextView)).setText(mTitle);
        ((TextView) layout.findViewById(R.id.introPagerTitleTextView)).setTypeface(tf);
        //-----------
        ((TextView)layout.findViewById(R.id.introPagerDescriptionTextView)).setText(mDescription);
        ((TextView)layout.findViewById(R.id.introPagerDescriptionTextView)).setTypeface(tf);
        //-----------------
        ((ImageView)layout.findViewById(R.id.introductionPageImageView)).setImageResource(mImageId);

        return layout;
    }

    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_DESCRIPTION, mDescription);
        outState.putString(KEY_TItle, mTitle);
        outState.putInt(KEY_IMAGEID, mImageId);
    }


}
