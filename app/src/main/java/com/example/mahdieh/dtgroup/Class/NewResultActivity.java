package com.example.mahdieh.dtgroup.Class;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mahdieh.dtgroup.Adapter.ResultActivityAdapter;
import com.example.mahdieh.dtgroup.Model.Flight;
import com.example.mahdieh.dtgroup.Other.JaliliCalendar;
import com.example.mahdieh.dtgroup.Other.Utils;
import com.example.mahdieh.dtgroup.R;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by mahdieh on 5/25/2016.
 */
public class NewResultActivity extends AppCompatActivity {

    List<Flight> flightItems = new ArrayList<Flight>();
    ResultActivityAdapter adapter;

    Boolean checkBox1, checkBox2;
    String StrOfCheckBox2, StrOfCheckBox1, StartSpinnerResult, DestinationSpinnerResult, spinner_adultResult,
            spinner_KidsResult, spinner_BabyResult, DateOfTravel;

    //=============== Initialize the RecyclerView ================
    @Bind(R.id.resultActivityRecyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Nullable
    @Bind(R.id.resultActivityTextViewNoflightAlert)
    TextView txtAlert;
    @Nullable
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    //===========================================================
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);

//======== extras values========================================
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            checkBox1 = getIntent().getExtras().getBoolean("checkBox1Value");
             StrOfCheckBox1 = String.valueOf(checkBox1);
             checkBox2 = getIntent().getExtras().getBoolean("checkBox2Value");
             StrOfCheckBox2 = String.valueOf(checkBox2);
             StartSpinnerResult = extras.getString("StartSpinnerSelected");
             DestinationSpinnerResult = extras.getString("destinationSpinnerSelected");
             spinner_adultResult = extras.getString("spinner_adultSelected");
             DateOfTravel = extras.getString("date");
             spinner_KidsResult = extras.getString("spinner_KidsSelected");
             spinner_BabyResult = extras.getString("spinner_BabySelected");
        }

        //===============Thread(new Runnable==================
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sendJsonRequest();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        //=============== Initialize the Adapter, Data Array and set Adapter ===================//
        flightItems = new ArrayList<>();
        adapter = new ResultActivityAdapter(this, flightItems);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(NewResultActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        //=================addOnItemTouchListener===============
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(NewResultActivity.this, FlightInfoActivity.class);
                Flight currentFlight = flightItems.get(position);
                //***************
                intent.putExtra("StartSpinnerResult", StartSpinnerResult);
                intent.putExtra("DestinationSpinnerResult", DestinationSpinnerResult);
                intent.putExtra("spinner_adultResult", spinner_adultResult);
                intent.putExtra("DateOfTravel",DateOfTravel);
                intent.putExtra("spinner_KidsResult" ,spinner_KidsResult );
                intent.putExtra("spinner_BabyResult", spinner_BabyResult);
                //****************
                intent.putExtra("airline", currentFlight.getAirline());
                intent.putExtra("num_flight",currentFlight.getNum_flight());
                intent.putExtra("startTripCity", currentFlight.getStartTripCity());
                intent.putExtra("capacity", currentFlight.getCapacity());
                intent.putExtra("destinationTripCity", currentFlight.getDestTripCity());
                intent.putExtra("Start_time", currentFlight.getTime());
                intent.putExtra("End_time", currentFlight.getTime());
                intent.putExtra("type", currentFlight.getType());
                intent.putExtra("week", currentFlight.getWeek());
                intent.putExtra("Carrier", currentFlight.getCarrier());
                //intent.putExtra("price", currentFlight.getPrice()); //
                intent.putExtra("price", currentFlight.getPrice());
                intent.putExtra("Link", currentFlight.getLink());
                startActivity(intent);
            }
            @Override
            public void onItemLongClick(View view, int position) { // 3 second long
                //empty
            }
        }));
    }

    //=======================sendJsonRequest =============================================
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void sendJsonRequest() throws IOException {
           OkHttpClient client = new OkHttpClient();
           //*********************
            HashMap<String, String> citiesList = Utils.getCityList();
                String Source_Key = citiesList.get(StartSpinnerResult);
                String Dest_key = citiesList.get(DestinationSpinnerResult);

         String[] arr = DateOfTravel.split("/");
        JaliliCalendar.YearMonthDate jalali = new JaliliCalendar.YearMonthDate(Integer.parseInt(arr[0]),
                Integer.parseInt(arr[1]) + 1, Integer.parseInt(arr[2]));
        JaliliCalendar.YearMonthDate gDate = JaliliCalendar.jalaliToGregorian(jalali);
                String dateFormatted =String.format("%d-%d-%d",gDate.getYear(), gDate.getMonth(),gDate.getDate());
                Log.d("ooo", dateFormatted);

               //====================
                HttpUrl.Builder urlBuilder =
                        HttpUrl.parse("http://charter724.ir/webservice/list_flight.php?id=213&key=3nN1iR74IK0Vokr81oJZ16r3l")
                                .newBuilder();
                urlBuilder.addQueryParameter("from", Source_Key);
                urlBuilder.addQueryParameter("to", Dest_key);
                urlBuilder.addQueryParameter("date", dateFormatted);
               Log.d("date", dateFormatted);
                //****************prepare URL **********************8
                String MyUrl = urlBuilder.build().toString();
                //****************
                RequestBody Result_Post_body = new FormBody.Builder()
                        .add("StrPostResult[0]", StartSpinnerResult)
                        .add("StrPostResult[1]", DestinationSpinnerResult)
                        .add("StrPostResult[2]", spinner_adultResult)
                        .add("StrPostResult[3]", spinner_KidsResult)
                        .add("StrPostResult[4]", spinner_BabyResult)
                        .add("StrPostResult[5]", dateFormatted) //??????????
                        .add("StrPostResult[6]", StrOfCheckBox2)
                        .add("StrPostResult[7]", StrOfCheckBox1)
                        .build();
                //********************Prepare request ******************
                Request request = new Request.Builder()
                        .url(MyUrl) //parse url
                        .post(Result_Post_body)
                        .build();
                //********************************************************
                Response response = client.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                final String JsonData = response.body().string(); //Get API response
                Log.d("test", JsonData);
                //===================================================try {
                //parse json
                try {
                    //get array
                    JSONArray FlightList = new JSONArray(JsonData);
                    for (int i = 0; i < FlightList.length(); i++) {
                        JSONObject currentFlight = FlightList.getJSONObject(i);
                        Flight flightResult = new Flight();
                        //==============================================
                        flightResult.setStartTripCity(StartSpinnerResult);
                        flightResult.setDestTripCity(DestinationSpinnerResult);
                        flightResult.setWeek(DateOfTravel);
                        //==================================================
                        flightResult.setAirline(currentFlight.getString("airline"));
                        flightResult.setCapacity(currentFlight.getInt("capacity"));
                        flightResult.setCarrier(currentFlight.getString("Carrier"));
                        flightResult.setExpid(currentFlight.getString("expid"));
                        flightResult.setId_flight(currentFlight.getString("id_flight"));
                        flightResult.setLink(currentFlight.getString("Link"));
                        flightResult.setNum_flight(currentFlight.getString("num_flight"));
                        flightResult.setRDB(currentFlight.getString("RBD"));

                        //===== todo:????
                        Integer Sum = Integer.valueOf(spinner_KidsResult) + Integer.valueOf(spinner_BabyResult) +
                                Integer.valueOf(spinner_adultResult);
                        Integer Temp= Integer.valueOf(currentFlight.getString("price").replaceAll("[^0-9]",""));
                        Integer updatedTemp= Temp * Sum;
                        flightResult.setPrice(String.valueOf(updatedTemp));
                        flightResult.setTime(currentFlight.getString("time"));
                        flightItems.add(flightResult);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (flightItems.isEmpty()){
                            recyclerView.setVisibility(View.GONE);
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(R.string.notFount);
                        }
                        else {
                            recyclerView.setVisibility(View.VISIBLE);
                            txtAlert.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();

                        }
                      recyclerView.setVisibility(View.VISIBLE);//
                       progressBar.setVisibility(View.GONE); //
                        adapter.notifyDataSetChanged();
                    }
                });

        }
    }


        //==========================================================
//    private void setupToolbar() {
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        toolbar.setTitleTextColor(Color.WHITE);
//        getSupportActionBar().setTitle(R.string.app_name);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.previous);
//    }







