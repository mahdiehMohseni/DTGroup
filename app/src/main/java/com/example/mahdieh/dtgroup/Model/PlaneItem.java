package com.example.mahdieh.dtgroup.Model;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by mahdieh on 5/9/2016.
 */
public class PlaneItem implements Comparator<PlaneItem> {

        @SerializedName("id")
        private int planeID;

        @SerializedName("name")
        private String planeName;

        public PlaneItem() {
        }

        public PlaneItem(int pleneID, String planeName) {

            this.planeName = planeName;
            this.planeID= planeID;
        }

   public int getPlaneID () {
        return planeID;
    }




    public void setPlaneID (int planeID) {
        this.planeID = planeID;
    }

    public String getPlaneName() {
        return planeName;
    }

       public void setPlaneName(String planeName){
           this.planeName = planeName;
       }


    @Override
    public int compare(PlaneItem lhs, PlaneItem rhs) {
        return 0;
    }
}
