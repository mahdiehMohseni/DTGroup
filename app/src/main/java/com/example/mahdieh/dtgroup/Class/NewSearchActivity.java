package com.example.mahdieh.dtgroup.Class;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mahdieh.dtgroup.Model.City;
import com.example.mahdieh.dtgroup.Other.Utils;
import com.example.mahdieh.dtgroup.R;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mahdieh on 5/24/2016.
 */
public class NewSearchActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @Bind(R.id.spinner_show1)
    Spinner sourceSpinner;
    @Bind(R.id.spinner_show2)
    Spinner destSpinner;
    @Bind(R.id.searchActivityDate1)
    Button dateBtn;
    @Bind(R.id.spinner_baby)
    Spinner spinner_baby;
    @Bind(R.id.spinner_kids)
    Spinner spinner_kids;
    @Bind(R.id.spinner_adult)
    Spinner spinner_adult;
    @Bind(R.id.searchActivityCheckBox1)
    CheckBox cancellableCheckBox;
    @Bind(R.id.searchActivityCheckBox2)
    CheckBox directCheckbox;
    @Bind(R.id.searchActivityButton)
    Button searchBtn;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        ButterKnife.bind(this);
        loadCities();
    }

    private void loadCities() {
        ArrayList<String> cities = new ArrayList<>();
        for (String key : Utils.getCities().keySet()) {
            City city = Utils.getCities().get(key);
            cities.add(city.getFaTitle());
        }

        sourceSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cities));
        destSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cities));
    }

    @OnClick(R.id.searchActivityButton)
    public void showResult(){
        String Selected_destSpinnerValue = destSpinner.getSelectedItem().toString();
        String Selected_sourceSpinnerValue = sourceSpinner.getSelectedItem().toString();
        if (Selected_destSpinnerValue == Selected_sourceSpinnerValue){
            AlertDialog alertDialog = new AlertDialog.Builder(
                    NewSearchActivity.this).create();
            alertDialog.setTitle(getResources().getString(R.string.alertTitle));
            alertDialog.setMessage(getResources().getString(R.string.alertEqual));
            alertDialog.setIcon(R.drawable.warning);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                }
            });
            alertDialog.show();
        }else {
            int Selected_AdultSpinnerValue = Integer.valueOf((String) spinner_adult.getSelectedItem());
            int Selected_KidsSpinnerValue = Integer.valueOf((String) spinner_kids.getSelectedItem());

            int SumResult = Selected_AdultSpinnerValue + Selected_KidsSpinnerValue; //Result
            if (SumResult >=9){
                AlertDialog alertDialog = new AlertDialog.Builder(
                        NewSearchActivity.this).create();
                alertDialog.setTitle(getResources().getString(R.string.alertTitle));
                alertDialog.setMessage(getResources().getString(R.string.sumofKidsAndAdultValue));
                alertDialog.setIcon(R.drawable.warning);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.show();
            }else {
                int Selected_AdultValue = Integer.valueOf((String) spinner_adult.getSelectedItem());
                int Selected_BabyValue = Integer.valueOf((String) spinner_baby.getSelectedItem());
                if (Selected_BabyValue > Selected_AdultValue){
                    AlertDialog alertDialog = new AlertDialog.Builder(NewSearchActivity.this).create();
                    alertDialog.setTitle(getResources().getString(R.string.alertTitle));
                    alertDialog.setMessage(getResources().getString(R.string.alertOnBabyAndAdultValue));
                    alertDialog.setIcon(R.drawable.warning);
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    });
                    alertDialog.show();
                }else {
                    Intent intent = new Intent(NewSearchActivity.this, NewResultActivity.class);
                    intent.putExtra("StartSpinnerSelected",sourceSpinner.getSelectedItem().toString());//StartTripSpinner.getSelectedItem().toString()
                    intent.putExtra("destinationSpinnerSelected", destSpinner.getSelectedItem().toString());//DestinationTripSpinner.getSelectedItem().toString()
                    intent.putExtra("spinner_adultSelected", spinner_adult.getSelectedItem().toString());//adultSpinner.getSelectedItem().toString()
                    intent.putExtra("spinner_KidsSelected", spinner_kids.getSelectedItem().toString());//kidsSpinner.getSelectedItem().toString()
                    intent.putExtra("spinner_BabySelected", spinner_baby.getSelectedItem().toString()); //babySpinner.getSelectedItem().toString()
                    intent.putExtra("checkBox1Value", cancellableCheckBox.isChecked());
                    intent.putExtra("checkBox2Value", directCheckbox.isChecked());
                    //=== date value
                    intent.putExtra("date", date);
                    //==== startActivity
                    startActivity(intent);
                }
            }
        }
    }

    @OnClick(R.id.searchActivityDate1)
    public void showDatePicker() {
        PersianCalendar myCal = new PersianCalendar();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, myCal.getPersianYear(), myCal.getPersianMonth(), myCal.getPersianDay());
        datePickerDialog.show(getFragmentManager(), null);
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = String.format("%d/%d/%d", year, monthOfYear, dayOfMonth);
        dateBtn.setText(date);
        view.dismiss();
    }
}