package com.example.mahdieh.dtgroup.Class;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mahdieh.dtgroup.Adapter.DrawerItemAdapter;
import com.example.mahdieh.dtgroup.Adapter.SideBarAdapter;
import com.example.mahdieh.dtgroup.Model.DrawerItem;
import com.example.mahdieh.dtgroup.Other.MyApplication;
import com.example.mahdieh.dtgroup.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainActivity extends AppCompatActivity {


    protected DrawerLayout mDrawerLayout;
    protected ActionBarDrawerToggle drawerToggle;
    protected ListView mDrawerList;
    protected Toolbar toolbar;

    protected LinearLayout mainLinearLayout;

    protected Typeface tfSocial;
    protected Typeface tfIcon;
    protected Typeface tf;

    protected float rippleSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tfIcon = Typeface.createFromAsset(getAssets(), "fonts/flaticon.ttf");
        tfSocial = Typeface.createFromAsset(getAssets(), "fonts/social.ttf");
        tf = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile.ttf");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.navdrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_ab_drawer);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            toolbar.setTitleTextColor(Color.WHITE);

            TextView toolbarTiltle = getToolbarTextView();
            if ( toolbarTiltle != null )
                toolbarTiltle.setTypeface(tf);
        }

        mDrawerList.setBackgroundColor(getResources().getColor(R.color.violet));
        SideBarAdapter adapter = new SideBarAdapter(this, R.layout.sidebar_list_item,
                getResources().getStringArray(R.array.sideBarItemTitles), getResources().getStringArray(R.array.sideBarItemIcons), tf, tfIcon);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                        case 0: //introduction.
                            startActivity(new Intent(MainActivity.this, IntroductionActivity.class));
                            break;
                        case 1: //rules and policy
                            startActivity(new Intent(MainActivity.this, PolicyActivity.class));
                            break;
                        case 2: //contactUS
                            startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
                            break;
                        case 3:// social channel

                            startActivity(new Intent(MainActivity.this, SocialContactActivity.class));
                            break;
                        case 4: //bug report
                            startActivity(new Intent(MainActivity.this, BugReportActivity.class));
                            break;
                    }

                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });


        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        }; // Drawer Toggle Object Made

        mDrawerLayout.setDrawerListener(drawerToggle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        rippleSpeed = size.x / 16;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    protected void showErrorDialog( String errorMsg ) {
        SweetAlertDialog errorDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.error))
                .setContentText(errorMsg);
        errorDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                ((Button) alertDialog.findViewById(R.id.confirm_button)).setText(getString(R.string.ok));
                ((Button) alertDialog.findViewById(R.id.confirm_button)).setTypeface(tf);
                ((TextView) alertDialog.findViewById(R.id.title_text)).setTypeface(tf);
                ((TextView) alertDialog.findViewById(R.id.content_text)).setTypeface(tf);
            }
        });
        errorDialog.show();
    }

    protected TextView getToolbarTextView() {
        TextView titleTextView = null;

        try {
            Field f = toolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(toolbar);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
        return titleTextView;
    }
}