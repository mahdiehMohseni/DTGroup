package com.example.mahdieh.dtgroup.Adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mahdieh.dtgroup.Class.DrawerItemHolder;
import com.example.mahdieh.dtgroup.Class.MainActivity;
import com.example.mahdieh.dtgroup.Model.DrawerItem;
import com.example.mahdieh.dtgroup.R;

import java.util.List;

/**
 * Created by mahdieh on 5/2/2016.
 */
public class DrawerItemAdapter extends RecyclerView.Adapter<DrawerItemAdapter.DataObjectHolder> {

    //slide menu items
    private List<DrawerItem> items;
    String[] titles;
    TypedArray icons;
    Context context;

    public DrawerItemAdapter(String[] itemsTitle, TypedArray icons, MainActivity mainActivity) {

    }

    /*
    public DrawerItemAdapter(String[] titles, TypedArray icons, Context context) {
        this.titles = titles;
        this.icons = icons;
        this.context = context;
    }
    */
/*
    public DrawerItemAdapter(List<DrawerItem> items) {

        this.items = items;
    }
    */
//Its a inner class to RecyclerViewAdapter Class.
public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView textView;
    ImageView imageView;
    Context context;

    public DataObjectHolder(View itemView, int itemType, Context context) {
        super(itemView);
        this.context=context;
        itemView.setOnClickListener(this);

        if (itemType==1){
            textView = (TextView) itemView.findViewById(R.id.drawer_text);
            imageView = (ImageView) itemView.findViewById(R.id.drawer_icon);

        }
    }

    @Override
    public void onClick(View v) {
        //MainActivity mainActivity = (MainActivity) context;
        //mainActivity.mDrawerLayout.closeDrawers();

    }
}


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_item_row_j, parent, false);
        //return new DataObjectHolder(itemView);
        //}
        if (viewType == 1) {
            View itemLayout = layoutInflater.inflate(R.layout.nav_item_row_j, null);
            return new DataObjectHolder(itemLayout, viewType, context);
        } else if (viewType == 0) {
            View itemHeader = layoutInflater.inflate(R.layout.header_nav, null);
            return new DataObjectHolder(itemHeader, viewType, context);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {

        if (position!=0) {
            holder.imageView.setImageResource(icons.getResourceId(position - 1, -1));
            holder.textView.setText(titles[position - 1]);
        }
    }




    @Override
    public int getItemCount() {
        return titles.length+1;
    }

    public int getItemViewType(int position){
        if (position==0) return 0;
        else return 1;
    }

    //--------------



}
