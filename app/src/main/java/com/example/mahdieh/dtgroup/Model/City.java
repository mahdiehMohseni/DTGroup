package com.example.mahdieh.dtgroup.Model;

/**
 * Created by mahdieh on 5/24/2016.
 */
public class City {

    String key,enTitle,faTitle;

    public City(String key, String enTitle, String faTitle) {
        this.key = key;
        this.enTitle = enTitle;
        this.faTitle = faTitle;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEnTitle() {
        return enTitle;
    }

    public void setEnTitle(String enTitle) {
        this.enTitle = enTitle;
    }

    public String getFaTitle() {
        return faTitle;
    }

    public void setFaTitle(String faTitle) {
        this.faTitle = faTitle;
    }
}
