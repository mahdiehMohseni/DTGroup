package com.example.mahdieh.dtgroup.Other;

import android.app.Application;
import android.util.Log;
import android.webkit.WebView;

import com.example.mahdieh.dtgroup.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by mahdieh on 6/1/2016.
 */
public class MyApplication extends Application {
    public static final String TAG = MyApplication.class.getSimpleName();
    private static MyApplication mInstance;

    public void onCreate(){
        super.onCreate();
        mInstance=this;

        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getsInstance().get(AnalyticsTrackers.Target.APP);
    }

    public static synchronized MyApplication getInstance(){
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker(){
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getsInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /**
     * Tracking screen view
     */
    public void trackScreenView(String screenName){
        Tracker t = getGoogleAnalyticsTracker();
        //set screen name

        Log.i(TAG, "Setting screen name: " + screenName);
        t.setScreenName(screenName);
        t.setScreenColors(String.valueOf(R.color.back_blue_light));
        //send a screen view
        t.send(new HitBuilders.ScreenViewBuilder().build());
        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }
    /**
     * Tracking exception
     */

    public void trackException (Exception e){
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();
            t.send(new HitBuilders.ExceptionBuilder().setDescription(new StandardExceptionParser(this, null)
                    .getDescription(Thread.currentThread().getName(), e)).setFatal(false).build()
            );
        }
    }
    /**
     * Tracking event
     */

    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();
        //build and send an event
        t.send(new HitBuilders.EventBuilder().setCategory(category)
                .setAction(action).setLabel(label).build());
        //t.enableAdvertisingIdCollection(true);
    }
}
