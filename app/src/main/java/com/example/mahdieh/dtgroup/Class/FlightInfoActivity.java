package com.example.mahdieh.dtgroup.Class;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mahdieh.dtgroup.R;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mahdieh on 5/16/2016.
 */
public class FlightInfoActivity extends AppCompatActivity {

    //===local Variables
    Typeface tf;
    int flight_info_capacity;
    String flightInfo_AdultSpinner, flightInfo_KidsSpinner, flightInfo_BabySpinner, flightInfo_DestSpinner,
            flightInfo_SourceSpinner, flightInfo_Date, flight_info_airline,
            flight_info_num, flight_info_source, flight_info_dest, flight_info_startTime,
            flight_info_EndTime, flight_info_Type,
            flight_info_price, flight_info_Carrier, flight_info_week, flight_payment_Link;

    //==========Bind Variables
    //price of flight
    @Bind(R.id.activityFlightInfoFlightPrice)
    TextView TextViewFlightPrice;
    //date and times
    @Bind(R.id.activityFlightInfoStartTime)
    TextView TextViewStartTime;
    @Bind(R.id.activityFlightInfoStartDate)
    TextView TextViewStartDate;
    @Bind(R.id.activityFlightInfoEndTime)
    TextView TextViewEndTime;
    @Bind(R.id.activityFlightInfoEndDate)
    TextView TextViewEndDate;
    //flight info
    @Bind(R.id.activityFlightInfoSource)
    TextView TextViewInfoSource;
    @Bind(R.id.activityFlightInfoAirline)
    TextView TextViewInfoAirline;
    @Bind(R.id.activityFlightInfoFlightType)
    TextView TextViewInfoFlightType;
    @Bind(R.id.activityFlightInfoDest)
    TextView TextViewInfoFlightDest;
    @Bind(R.id.activityFlightInfoFlightNumber)
    TextView TextViewFlightInfoFlightNumber;
    @Bind(R.id.activityFlightInfoCarrier)
    TextView TextViewFlightInfoCarrier;
    //CancelPoliocies
    @Bind(R.id.flightInfoActivityTextViewCancelDescription)
    TextView TextViewFlightCancelDescription;
    @Bind(R.id.flightInfoActivityTextViewCancelTitle)
    TextView TextViewFlightCancelTitle;

    //final static values
    @Bind(R.id.activityFlightInfoUnit)
    TextView TextViewFlightPriceUnit;
//    @Bind(R.id.FlightInfoTextViewPricing2) //????
//    TextView TextViewFlightPricing;
    @Bind(R.id.flightInfoActivityTextViewInfoTitle)
    TextView TextViewFlightInfoTitle;

    //spinner values
    @Bind(R.id.activityFlightInfoAdultNumber)
    TextView TextViewFlightInfoAdultNumber;
    @Bind(R.id.activityFlightInfoKidsNumber)
    TextView TextViewFlightInfoKidsNumber;
    @Bind(R.id.activityFlightInfoBabyNumber)
    TextView TextViewFlightInfoBabyNumber;
    @Bind(R.id.activtyFlightInfoFlightLine)
    TextView TextViewFlightInfoFlightLine;

    //ribbon
    @Bind(R.id.FlightInfoTextViewPricing2)
    TextView TextViewOnLineBooking;
    @Bind(R.id.activity_flightInfo_ImageView_babyIcon)
    ImageView ImageViewBabyIcon;
    @Bind(R.id.activity_flightInfo_ImageView_ManIcon)
    ImageView ImageViewManIcon;
    @Bind(R.id.activity_flightInfo_ImageView_kidIcon)
    ImageView ImageViewKidsIcon;

    //=======================
      protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_flight_info);
          //====================
          ButterKnife.bind(this);
          //================ setTypesFaceForALL =====
          setTypesFaceForALL();
          tf = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile.ttf");
          //=====================
          final Bundle extras = getIntent().getExtras();
        if(extras!=null)
        {
            //================ get value using intent
            flightInfo_AdultSpinner = extras.getString("spinner_adultResult");
            flightInfo_KidsSpinner= extras.getString("spinner_KidsResult");
            flightInfo_BabySpinner = extras.getString("spinner_BabyResult");
            flightInfo_DestSpinner = extras.getString("DestinationSpinnerResult");
            flightInfo_SourceSpinner = extras.getString("StartSpinnerResult");
            flightInfo_Date = extras.getString("DateOfTravel");
            //************************
            flight_info_airline = extras.getString("airline");
            flight_info_num = extras.getString("num_flight");
            flight_info_source = extras.getString("startTripCity");
            flight_info_dest = extras.getString("DestTripCity");
            flight_info_startTime = extras.getString("Start_time");
            flight_info_EndTime = extras.getString("End_time");
            //flight_info_Type = extras.getString("type");
            flight_info_capacity = extras.getInt("capacity");
            flight_info_price = extras.getString("price");
            flight_info_Carrier = extras.getString("Carrier");
            flight_info_week = extras.getString("week");
            flight_payment_Link = extras.getString("Link");

//initialize and set values ==========================================
            TextViewFlightPrice.setText(flight_info_price);
            TextViewEndTime.setText(flight_info_EndTime);
            TextViewStartDate.setText(flightInfo_Date);

            TextViewInfoSource.setText(flightInfo_SourceSpinner);
            TextViewInfoAirline.setText(flight_info_airline);
            TextViewInfoFlightType.setText(flight_info_Type);
            TextViewInfoFlightDest.setText(flightInfo_DestSpinner);
            TextViewFlightInfoFlightNumber.setText(flight_info_num);
            TextViewFlightInfoKidsNumber.setText(flightInfo_KidsSpinner);
            TextViewFlightInfoBabyNumber.setText(flightInfo_BabySpinner);
            TextViewFlightInfoCarrier.setText(flight_info_Carrier);
            TextViewFlightInfoAdultNumber.setText(flightInfo_AdultSpinner);

            //********** drawable icons **********************
            TextViewFlightInfoFlightLine.setBackgroundResource(R.drawable.flight_line);
            ImageViewBabyIcon.setBackgroundResource(R.drawable.baby);
            ImageViewKidsIcon.setBackgroundResource(R.drawable.kid);
            ImageViewManIcon.setBackgroundResource(R.drawable.standing_man);
            TextViewStartTime.setText(getResources().getString(R.string.DateOfFlight));
            TextViewEndDate.setText(getResources().getString(R.string.timeOfFlight));

            //====== initialize static value
            TextViewFlightPriceUnit.setText(getResources().getString(R.string.unit));
            //TextViewFlightPricing.setText(getResources().getString(R.string.onlineBooking));
            TextViewFlightInfoTitle.setText(getResources().getString(R.string.flightInfoTitle));
            TextViewOnLineBooking.setText(getResources().getString(R.string.OnlineBookingBtn));
            TextViewFlightCancelTitle.setText(getResources().getString(R.string.flighInfoActivityCancellingTitle));
            TextViewFlightCancelDescription.setText(getResources().getString(R.string.ruleOrg));
            //=============onclick listener on OnLineBookingBtn
            TextViewOnLineBooking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FlightInfoActivity.this, FlightBillingActivity.class);
                    intent.putExtra("flight_airline", flight_info_airline);
                    intent.putExtra("flight_source", flight_info_source);
                    intent.putExtra("flight_dest", flightInfo_DestSpinner);
                    intent.putExtra("flight_price", flight_info_price);
                    intent.putExtra("flight_Date", flightInfo_Date);
                    intent.putExtra("flight_startTime", flight_info_startTime);
                    intent.putExtra("flight_EndTime", flight_info_EndTime);
                    intent.putExtra("Kids_spinner_result", flightInfo_KidsSpinner);
                    intent.putExtra("Baby_spinner_result", flightInfo_BabySpinner);
                    intent.putExtra("Adult_spinner_result", flightInfo_AdultSpinner);
                    intent.putExtra("flight_num", flight_info_num);
                    intent.putExtra("flight_Carrier", flight_info_Carrier);
                    intent.putExtra("flight_payment_Link", flight_payment_Link);
                    startActivity(intent);
                }
            });
        }
      }
    //================setTypesFaceForALL==================
    private void setTypesFaceForALL() {
        TextViewFlightPrice.setTypeface(tf);
        TextViewStartTime.setTypeface(tf);
        TextViewEndTime.setTypeface(tf);
        TextViewStartDate.setTypeface(tf);
        TextViewEndDate.setTypeface(tf);
        TextViewInfoSource.setTypeface(tf);
        TextViewInfoAirline.setTypeface(tf);
        TextViewInfoFlightType.setTypeface(tf);
        TextViewInfoFlightDest.setTypeface(tf);
        TextViewFlightInfoFlightNumber.setTypeface(tf);
        TextViewFlightInfoAdultNumber.setTypeface(tf);
        TextViewFlightInfoKidsNumber.setTypeface(tf);
        TextViewFlightInfoBabyNumber.setTypeface(tf);
        TextViewFlightInfoCarrier.setTypeface(tf);
        //------------------------
        TextViewFlightPriceUnit.setTypeface(tf);
        TextViewFlightInfoTitle.setTypeface(tf);
        TextViewFlightCancelTitle.setTypeface(tf);
        TextViewFlightCancelDescription.setTypeface(tf);
        TextViewOnLineBooking.setTypeface(tf);
    }
}
